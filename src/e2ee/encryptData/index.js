var _kms = require('@google-cloud/kms')

async function encryptData(keyId, unEncryptedData) {
	const projectId = 'soundheart-dev-94cc1'
	const locationId = 'us-east1'
	const keyRingId = 'soundheart-keys'

	const plaintextBuffer = Buffer.from(JSON.stringify(unEncryptedData))
	const client = new _kms.KeyManagementServiceClient()
	const keyName = client.cryptoKeyPath(projectId, locationId, keyRingId, keyId)
	const [encryptResponse] = await client.encrypt({
		name: keyName,
		plaintext: plaintextBuffer,
	})
	if (!encryptResponse.ciphertext) throw new Error('failed-to-encrypt')
	return Buffer.from(encryptResponse.ciphertext ?? '').toString('base64')
}

module.exports = { encryptData }
