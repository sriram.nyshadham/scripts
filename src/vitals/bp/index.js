const { addVitalToPatient } = require('..')
const data = require('./bp.json')
const _ = require('lodash')
const { db } = require('../../admin')

async function addBpRecords() {
	const bpRecords = []
	Object.entries(data).forEach(([patientId, vitals]) => {
		vitals.forEach((vital) => bpRecords.push({ ...vital, patientId }))
	})
	const batches = _.chunk(bpRecords, 10)
	const results = []
	while (batches.length) {
		const batch = batches.shift()
		const result = await Promise.all(
			batch.map((vital) =>
				addVitalToPatient(vital, vital['patientId'], 'BP_RECORD')
			)
		)
		results.push(result)
	}
	console.log(_.flatten(results))
}

addBpRecords()
