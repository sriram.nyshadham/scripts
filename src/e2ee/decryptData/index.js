var _kms = require('@google-cloud/kms')

async function decryptData(keyId, encryptedData) {
	const projectId = 'soundheart-dev-94cc1'
	const locationId = 'us-east1'
	const keyRingId = 'soundheart-keys'
	const ciphertext = Buffer.from(encryptedData, 'base64')

	const client = new _kms.KeyManagementServiceClient()
	const keyName = client.cryptoKeyPath(projectId, locationId, keyRingId, keyId)
	const [decryptResponse] = await client.decrypt({
		name: keyName,
		ciphertext: ciphertext,
	})
	return decryptResponse.plaintext
}

module.exports = {
	decryptData,
}
