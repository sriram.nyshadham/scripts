const { default: axios } = require('axios')
const { db } = require('../admin')
const { bpRules, weightRules } = require('./data')

async function updateRules() {
	const batch = db.batch()
	return db
		.collection('PATIENTS')
		.get()
		.then((querySnapshot) => {
			querySnapshot.docs.forEach((doc) => {
				const ref = db.collection(`PATIENTS/${doc.id}/VITAL_RULES`)
				const bpRef = ref.doc('blood-pressure')
				const weightRef = ref.doc('weight')
				batch.set(bpRef, { ...bpRules, id: bpRef.id, patientId: doc.id })
				batch.set(weightRef, {
					...weightRules,
					id: weightRef.id,
					patientId: doc.id,
				})
			})
			return batch.commit()
		})
		.then((res) => {
			return console.log({ res })
		})
		.catch((err) => {
			console.error(err)
		})
}

async function updatePatient(patientId) {
	const querySnapshot = await db
		.collection(`PATIENTS/${patientId}/BP_RECORD`)
		.orderBy('createdAt', 'desc')
		.limit(1)
		.get()

	if (querySnapshot.size < 1) throw new Error('no-bp-records')
	const latestBpRecord = querySnapshot.docs[0].data()
	const documentSnapshot = await db.doc(`SYMMETRIC_KEYS/${patientId}`).get()
	if (!documentSnapshot.exists) throw new Error('no-patient-found')
	const decryptedResponse = await axios.post(
		'http://localhost:8000/decrypt_data',
		{
			keyId: documentSnapshot.data()?.keyId,
			encryptedData: latestBpRecord.encryptedData,
		}
	)
	const decryptedBpRecord = JSON.parse(decryptedResponse.data.data)
	const verifyBpResponse = await axios.post(
		`http://localhost:5001/soundheart-dev-94cc1/us-central1/api/alerts/bp/${patientId}`,
		decryptedBpRecord
	)
	console.log({
		[patientId]: {
			decryptedResponse,
			decryptedBpRecord,
			verifyBpResponse,
		},
	})
}

async function updateAlertBasedOnBp() {
	const promises = []
	return db
		.collection('PATIENTS')
		.get()
		.then((querySnapshot) => {
			querySnapshot.docs.forEach((doc) => {
				const promise = updatePatient(doc.id)
				promises.push(promise)
			})
			return Promise.allSettled(promises)
		})
		.then((allSettledRes) => {
			allSettledRes.forEach((promiseRes) => {
				if (promiseRes.status === 'rejected') {
					console.error(promiseRes.reason)
				}
			})
		})
		.catch((err) => {
			console.error(err)
		})
}

updateAlertBasedOnBp()
