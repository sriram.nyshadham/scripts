const bpRules = {
	desired: {
		systolic: {
			min: 120,
			max: 120,
		},
		diastolic: {
			min: 80,
			max: 80,
		},
		operator: 'and',
	},
	elevated: {
		systolic: {
			min: 120,
			max: 129,
		},
		diastolic: {
			min: 0,
			max: 79,
		},
		operator: 'and',
	},
	stage1: {
		systolic: {
			min: 130,
			max: 139,
		},
		diastolic: {
			min: 80,
			max: 89,
		},
		operator: 'or',
	},
	stage2: {
		systolic: {
			min: 140,
			max: 999,
		},
		diastolic: {
			min: 90,
			max: 999,
		},
		operator: 'or',
	},
	units: 'mmHg',
}

const weightRules = {
	desired: {
		weight: {
			min: 118,
			max: 148,
		},
	},
	overWeight: {
		weight: {
			min: 155,
			max: 179,
		},
	},
	obesity: {
		weight: {
			min: 186,
			max: 241,
		},
	},
	severeObesity: {
		weight: {
			min: 247,
			max: 334,
		},
	},
	units: 'pounds',
}

module.exports = { bpRules, weightRules }
