const { default: axios } = require('axios')
const { db, admin } = require('../admin')
const { encryptData } = require('../e2ee/encryptData')

async function addVitalToPatient(vital, patientId, vitalName) {
	console.log(`adding vital for the patient ${patientId}`)
	const record = {
		sys: vital['sys'],
		dia: vital['dia'],
		deviceId: vital['deviceId'],
		isManual: vital['isManual'],
	}
	console.log(`fetching symmetric key for ${patientId}`)
	const documentSnapshot = await db.doc(`SYMMETRIC_KEYS/${patientId}`).get()
	if (!documentSnapshot.exists) throw new Error('no-patient-found')
	console.log(`encrypting vitals for ${patientId}`)
	const encryptedData = await encryptData(
		documentSnapshot.data()?.keyId,
		record
	)

	console.log(`encryption completed for ${patientId}`)

	const ref = db.collection(`PATIENTS/${patientId}/${vitalName}`).doc()
	const res = await ref.set({
		id: ref.id,
		isExist: true,
		encryptedData,
		createdBy: patientId,
		createdAt: admin.firestore.Timestamp.fromDate(new Date(vital['createdAt'])),
	})
	console.log(`vitals added ${patientId}`)
	return res
}

async function deleteVitals(vital) {
	const batch = db.batch()
	const qs = await db.collectionGroup(vital).get()
	qs.docs.forEach((doc) => {
		batch.delete(doc.ref)
	})
	await batch.commit()
}

module.exports = { addVitalToPatient, deleteVitals }
