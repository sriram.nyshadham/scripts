const { db } = require('../admin')
const fs = require('fs')

async function getPatientsUids() {
	const querySnapshot = await db.collection('PATIENTS').get()
	const ids = []
	querySnapshot.docs.forEach((doc) => ids.push(doc.id))
	fs.writeFile('./data.json', JSON.stringify(ids), (err) => {
		if (!err) {
			return console.log('completed')
		}
		console.error(err)
	})
}

getPatientsUids()
