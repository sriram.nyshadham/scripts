const { addVitalToPatient } = require('..')
const data = require('./pulse.json')
const _ = require('lodash')

async function addPulseRecords() {
	const pulseRecords = []
	Object.entries(data).forEach(([patientId, vitals]) => {
		vitals.forEach((vital) => pulseRecords.push({ ...vital, patientId }))
	})
	const batches = _.chunk(pulseRecords, 10)
	const results = []
	while (batches.length) {
		const batch = batches.shift()
		const result = await Promise.all(
			batch.map((vital) =>
				addVitalToPatient(vital, vital['patientId'], 'PULSE')
			)
		)
		results.push(result)
	}
	console.log(_.flatten(results))
}

addPulseRecords()
