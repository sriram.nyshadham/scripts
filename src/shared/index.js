const _ = require('lodash')

const BATCH_SIZE = 10

async function resolvePromises(promises) {
	const batches = _.chunk(promises, BATCH_SIZE)
	const results = []
	while (batches.length) {
		const batch = batches.shift()
		const result = await Promise.all(batch)
		results.push(result)
	}
	return _.flatten(results)
}

module.exports = { resolvePromises }
