const { default: axios } = require('axios')
const _ = require('lodash')
const { db, admin } = require('../admin')

const BATCH_SIZE = 10

async function importPatients() {
	const data = require('./data.json')
	const batches = _.chunk(data, BATCH_SIZE)
	const results = []
	while (batches.length) {
		const batch = batches.shift()
		const result = await Promise.allSettled(
			batch.map((patientRecord) => createPatient(patientRecord))
		)
		results.push(
			result.map((item) => {
				if (item.status === 'rejected') return item.reason
				return item.value
			})
		)
	}
	console.log(_.flatten(results))
}

async function createPatient(patient) {
	const phoneNumber = patient.mobile.replace(/\s/g, '')
	admin
		.auth()
		.getUserByPhoneNumber(phoneNumber)
		.then((userRecord) => {
			console.log(`user found ${userRecord.uid}`)
			return admin.auth().deleteUser(userRecord.uid)
		})
		.catch((err) => {
			if (err.code === 'auth/user-not-found') {
				// user doesn't exist, create it...
				console.log(`creating user..`)
				return admin.auth().createUser({
					phoneNumber: phoneNumber,
					password: 'soundheart123$',
				})
			}
		})
		.then((userRecord) => {
			console.log(`creating user ${userRecord.uid}`)
			return axios.post(
				`https://us-central1-soundheart-dev-94cc1.cloudfunctions.net/api/auth/newpatient`,
				{
					...patient,
					uid: userRecord.uid,
				}
			)
		})
}

importPatients()
